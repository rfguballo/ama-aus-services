package com.aus.amaysim.main;

import org.junit.After;
import org.junit.Before;

import com.aus.amaysim.openqa.core.BaseTest;
import com.aus.amaysim.openqa.pageobjects.BrowserPage;
import com.aus.amaysim.openqa.pageobjects.HomePage;
import com.aus.amaysim.openqa.pageobjects.LoginPage;
import com.aus.amaysim.utilities.dataprocess.GetData;

/**
 * @author rguballo
 *
 */
public class LoginSession extends BaseTest {
	
	@Before
	public void userLogin() {
		
		LoginPage.LoginForm.login_Account.VERIFY_IF_DISPLAYED();
		LoginPage.LoginForm.login_Account.CLICK();
		
		LoginPage.LoginForm.username.VERIFY_IF_DISPLAYED();
		LoginPage.LoginForm.username.INSERT_TEXT(GetData.executeInstance().username("username"));
		
		LoginPage.LoginForm.password.VERIFY_IF_DISPLAYED();
		LoginPage.LoginForm.password.INSERT_TEXT(GetData.executeInstance().password("password"));
		
		LoginPage.LoginForm.login.VERIFY_IF_DISPLAYED();
		LoginPage.LoginForm.login.VERIFY_IF_ENABLED();
		LoginPage.LoginForm.login.CLICK();
	}
	
	@After
	public void closeBrowser() {
		
		HomePage.Header.logout.VERIFY_IF_DISPLAYED();
		HomePage.Header.logout.VERIFY_IF_ENABLED();
		HomePage.Header.logout.CLICK();
		
		BrowserPage.QUIT_BROWSER();
	}
}
