package com.aus.amaysim;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.aus.amaysim.openqa.core.BaseTest;
import com.aus.amaysim.utilities.report.TestInfo;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author rguballo
 *
 */
public class CommandExecutor extends BaseTest {
	
	private static final Logger LOGGER = Logger.getLogger(CommandExecutor.class);
	
	private static CommandExecutor navElement;

	private CommandExecutor() {
	}

	public static CommandExecutor executeInstance() {
		if (navElement == null) {
			navElement = new CommandExecutor();
		}
		return navElement;
	}
	
	public void runURL(String url) {
		
		try {
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			TestInfo.getTest().log(LogStatus.INFO, "Browser successfully launched...");
			
			driver.get(url);
			TestInfo.getTest().log(LogStatus.INFO, "Browser successfully load the url...");
			
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
	}
}
