package com.aus.amaysim.scripts.PlanPage;

import org.junit.Test;

import com.aus.amaysim.main.LoginSession;
import com.aus.amaysim.openqa.pageobjects.HomePage;

/**
 * @author rguballo
 * @description: Testing Plan Page
 *
 */
public class PLP_TC01 extends LoginSession {

	@Test
	public void testHomePage() {
		HomePage.PlanContent.plan.VERIFY_IF_DISPLAYED();
		HomePage.PlanContent.notifications.VERIFY_IF_DISPLAYED();
		
		HomePage.MenuContainer.service_Primary_Info.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.service_Secondary_Info.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.myDashboard.VERIFY_IF_ENABLED();
	}
}
