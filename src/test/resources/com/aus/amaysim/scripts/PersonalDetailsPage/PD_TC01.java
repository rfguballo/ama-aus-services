package com.aus.amaysim.scripts.PersonalDetailsPage;

import org.junit.Test;

import com.aus.amaysim.main.LoginSession;
import com.aus.amaysim.openqa.pageobjects.BrowserPage;
import com.aus.amaysim.openqa.pageobjects.HomePage;
import com.aus.amaysim.utilities.dataprocess.GetData;

/**
 * @author: rguballo
 * @description: Verify Personal Details Functionalities
 *
 */
public class PD_TC01 extends LoginSession {

	@Test
	public void testPersonalDetails() {
		
		HomePage.MenuContainer.PersonalDetails.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.PersonalDetails.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.personalInfo_SectionHeader.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.fullName_Value.VERIFY_TEXT(GetData.executeInstance().fullName("full_name"));
		HomePage.PersonalDetailsContent.dataofBirth_Value.VERIFY_TEXT(GetData.executeInstance().dateOfBirth("date_of_birth"));
		
		HomePage.PersonalDetailsContent.contactInfo_SectionHeader.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.editContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.editContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.cancel_EditContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.cancel_EditContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.editContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.editContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.phoneNumber_Textbox.CLEAR_TEXT();
		HomePage.PersonalDetailsContent.emailAddress_Textbox.CLEAR_TEXT();
		HomePage.PersonalDetailsContent.saveContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.saveContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.phoneNumber_ErrorMessage.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.phoneNumber_ErrorMessage.VERIFY_TEXT("Please enter a valid contact number");
		HomePage.PersonalDetailsContent.emailAddress_ErrorMessage.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.emailAddress_ErrorMessage.VERIFY_TEXT("can't be blank");
		HomePage.PersonalDetailsContent.cancel_EditContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.cancel_EditContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.editContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.editContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();

		HomePage.PersonalDetailsContent.phoneNumber_Textbox.CLEAR_TEXT();
		HomePage.PersonalDetailsContent.emailAddress_Textbox.CLEAR_TEXT();
		HomePage.PersonalDetailsContent.phoneNumber_Textbox.INSERT_TEXT("473216487632187");
		HomePage.PersonalDetailsContent.emailAddress_Textbox.INSERT_TEXT("testuseraccount@");
		HomePage.PersonalDetailsContent.saveContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.phoneNumber_ErrorMessage.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.phoneNumber_ErrorMessage.VERIFY_TEXT("Please enter a valid contact number");
		HomePage.PersonalDetailsContent.emailAddress_ErrorMessage.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.emailAddress_ErrorMessage.VERIFY_TEXT("Please enter a valid email address");
		HomePage.PersonalDetailsContent.cancel_EditContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.cancel_EditContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.editContact.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.editContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.phoneNumber_Textbox.CLEAR_TEXT();
		HomePage.PersonalDetailsContent.emailAddress_Textbox.CLEAR_TEXT();
		HomePage.PersonalDetailsContent.phoneNumber_Textbox.INSERT_TEXT(GetData.executeInstance().phoneNumber("phone_number"));
		HomePage.PersonalDetailsContent.emailAddress_Textbox.INSERT_TEXT(GetData.executeInstance().emailAddress("email_address"));
		HomePage.PersonalDetailsContent.saveContact.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.SuccessfulModal.success_Header.VERIFY_IF_DISPLAYED();
		HomePage.SuccessfulModal.closeRevealModal.CLICK();
		
		HomePage.PersonalDetailsContent.phoneNumber_Value.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.phoneNumber_Value.VERIFY_TEXT(GetData.executeInstance().phoneNumber("phone_number"));
		HomePage.PersonalDetailsContent.emailAddress_Value.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.emailAddress_Value.VERIFY_TEXT(GetData.executeInstance().emailAddress("email_address"));
		
		
		HomePage.PersonalDetailsContent.editResidential.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.editResidential.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.editResidentialAddress.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.editResidentialAddress.INSERT_TEXT(GetData.executeInstance().address("address"));
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.cancel_EditResidential.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.cancel_EditResidential.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.editDelivery.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.editDelivery.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.editDeliveryAddress.VERIFY_IF_DISPLAYED();
		HomePage.PersonalDetailsContent.editDeliveryAddress.INSERT_TEXT(GetData.executeInstance().address("address"));
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.cancel_EditDelivery.VERIFY_IF_ENABLED();
		HomePage.PersonalDetailsContent.cancel_EditDelivery.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PersonalDetailsContent.residential_Value.VERIFY_TEXT(GetData.executeInstance().residential("residential"));
		HomePage.PersonalDetailsContent.delivery_Value.VERIFY_TEXT(GetData.executeInstance().delivery("delivery"));
		
	}
}
