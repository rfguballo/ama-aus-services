package com.aus.amaysim.utilities.constant;

/**
 * @author rguballo
 *
 */
public interface DriverPath {

	public static final String CHROME_DRIVER_PATH = "D:\\amaysim-automation-workspace\\amaysim-sydney-herokuapp\\driver\\chromedriver.exe";
}
