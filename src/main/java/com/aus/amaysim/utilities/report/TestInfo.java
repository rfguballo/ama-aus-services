package com.aus.amaysim.utilities.report;

import java.util.HashMap;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author rguballo
 *
 */
public class TestInfo {

	 static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
	 private static ExtentReports extent = ExtentManager.getInstance();

	 /**
	 * Get Extent Test value for reporting.
	 * 
	 * @return - extentTestMap
	 */
	public static synchronized ExtentTest getTest() {
	     return extentTestMap.get((int) (Thread.currentThread().getId()));
	 }

	 /**
	 * End report test execution.
	 */
	public static synchronized void endTest() {
	     extent.endTest(extentTestMap.get((int) (Thread.currentThread().getId())));
	 }

	 /**
	 * Log report for test case details: Test Name and Test Author 
	 * 
	 * @param testName
	 * @param author
	 * @return - startTestNameDetails
	 */
	public static synchronized ExtentTest startTestNameDetails(String testName, String author) {
		ExtentTest test = extent.startTest(testName, "Test Execution Report").assignAuthor(author);
		extentTestMap.put((int) (Thread.currentThread().getId()), test);

	     return test;
	 }

	 /**
	 * Log report for test case details: Test Name, Test Description and Test Author
	 * 
	 * @param testName
	 * @param desc
	 * @param author
	 * @return - test
	 */
	public static synchronized ExtentTest startTestNameDetails(String testName, String desc, String author) {
	     ExtentTest test = extent.startTest(testName, desc).assignAuthor(author);
	     extentTestMap.put((int) (Thread.currentThread().getId()), test);

	     return test;
	 }
}
