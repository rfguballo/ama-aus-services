package com.aus.amaysim.utilities.report;

/**
 * @author rguballo
 *
 */
public enum ReportStatusLabel {

	PASSED,
	FAILED,
	INFO,
	ERROR,
	WARNING,
	FATAL
	
}
