package com.aus.amaysim.utilities.report;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.aus.amaysim.openqa.core.BaseTest;

/**
 * @author rguballo
 *
 */
public class ScreenTestCapture extends BaseTest {

	private static final Logger LOGGER = Logger.getLogger(ScreenTestCapture.class);
	
	/**
	 * Method used to screen caps an active window of browser. It has a return
	 * type String that contains the absolute path of the created image
	 * 
	 * @param navigator
	 * @param ImageFilePath
	 * @return String - absolute path of the image
	 */

	public static String TakeSnapShot(String imageFilePath) {
		
		try {

		 //--> Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot =((TakesScreenshot)driver);
 
        //--> Call getScreenshotAs method to create image file
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
 
        //--> Move image file to new destination
        File DestFile=new File(imageFilePath);
               
        //--> Copy file at destination
        FileUtils.copyFile(SrcFile, DestFile);
        
		} catch (IOException e) {
			LOGGER.error("Failed to capture error image.");
		}
		return imageFilePath;
	}
}
