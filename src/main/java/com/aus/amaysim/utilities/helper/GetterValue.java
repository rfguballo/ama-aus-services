package com.aus.amaysim.utilities.helper;

/**
 * @author rguballo
 *
 */
public class GetterValue {
	
	private static String getSelect;

	public static String getGetSelect() {
		return getSelect;
	}

	public static void setGetSelect(String getSelect) {
		GetterValue.getSelect = getSelect;
	}
}
