package com.aus.amaysim.utilities.helper;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author rguballo
 *
 */
public class DateFormatUtils {
	
	/**
	 * Month output: June
	 * 
	 * @return - month
	 */
	public static String dateMonth(){
		
		Format formatter = new SimpleDateFormat("MMMM"); 
	    String month = formatter.format(new Date());
	    return month;	
	}
	
	/**
	 * Day output: 15
	 * 
	 * @return - dayValue
	 */
	public static String dateDayNo(){
		
		Calendar cal = Calendar.getInstance();
		int dayNo = cal.get(Calendar.DAY_OF_MONTH);
		String day = String.valueOf(dayNo);
		
		String dayValue = day;
		return dayValue;
	}
	
	/**
	 * Day output: Wednesday
	 * 
	 * @return - dayValue
	 */
	public static String dateDayName(){
		
		Calendar cal = Calendar.getInstance();
		Locale locale = Locale.getDefault();
		String dayName = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale);

		String dayValue = dayName;
		return dayValue;
	}
	
	/**
	 * Year output: 2016
	 * 
	 * @return - year
	 */
	public static String dateYear(){
		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.getWeekYear());
		
		return year;
	}
	
	/**
	 * Time output: 02:24 PM
	 * 
	 * @return - _12HrsTime
	 */
	public static String timeFormat12hrs(){
		try {       
			   DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss aa");
			   Calendar cal = Calendar.getInstance();
		 	   String _24HrsTime = dateFormat.format(cal.getTime());
		 	   
			   SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	           SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm:ss a");
	           Date _24HourDt = _24HourSDF.parse(_24HrsTime);
	           
	           String _12HrsTime = _12HourSDF.format(_24HourDt);
	           
	           return _12HrsTime;
	           
	       } catch (Exception e) {
	           e.printStackTrace();
	       }
		return null;
	}
	
	/**
	 * Time output: 22:15:44 PM
	 * 
	 * @return - time
	 */
	public static String timeFormat24hrs(){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss aa");
		Calendar cal = Calendar.getInstance();
		String time = StringQaHelper.append(dateFormat.format(cal.getTime()));
		
		return time;
	}
	
	public static String dateFormat(){
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		String date = sdf.format(new java.util.Date());
		
		return date;
	}
	
	/**
	 * Date output: Jun1320167146
	 * 
	 * @return - date
	 */
	public static String dateFormatWithTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("MMMdyhms");
		String date = sdf.format(new java.util.Date());
		
		return date;
	}
	
	/**
	 * Get The current date
	 */
	public static String getCurrentDate() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd : HH:mm:ss Sss"));
	}
}

