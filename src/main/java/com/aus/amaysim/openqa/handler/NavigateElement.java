package com.aus.amaysim.openqa.handler;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.aus.amaysim.openqa.core.BaseTest;
import com.aus.amaysim.utilities.helper.StringQaHelper;
import com.aus.amaysim.utilities.report.ExtentManager;
import com.aus.amaysim.utilities.report.ReportStatusLabel;
import com.aus.amaysim.utilities.report.TestInfo;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author rguballo
 *
 */
public class NavigateElement extends BaseTest {
	
	private static final Logger LOGGER = Logger.getLogger(NavigateElement.class);
	
	private static NavigateElement navElement;
	

	private NavigateElement() {
	}

	public static NavigateElement executeInstance() {
		if (navElement == null) {
			navElement = new NavigateElement();
		}
		return navElement;
	}
	
	public void runURL(String url) {
		
		try {
			
			driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			TestInfo.getTest().log(LogStatus.INFO, "Browser successfully launched...", ExtentManager.statusLableStyle(ReportStatusLabel.INFO));
			LOGGER.info("Browser successfully launched...");
			
			driver.get(url);
			TestInfo.getTest().log(LogStatus.INFO, "Browser successfully load the url...", ExtentManager.statusLableStyle(ReportStatusLabel.INFO));
			LOGGER.info("Browser successfully load the url...");
			
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
			
		}
	}
	
	public void quitBrowser() {
		
		try {
			
			driver.quit();
			
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void watiForAjaxToLoad() {
		
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("*//div[contains(@id, 'ajax_loading')][contains(@style, 'block')]")));
			
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	public void verifyEnabledElement(final By locator) {
		
		try {

			if (driver.findElement(locator).isEnabled()) {
				TestInfo.getTest().log(LogStatus.PASS, "Element: " + locator + " is enabled", ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));
			} else {
				TestInfo.getTest().log(LogStatus.FAIL, "Element: " + locator + " is disabled...", ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
				Assert.fail("Element is disabled...");
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void verifyDisabledElement(final By locator) {
		try {
			
			if (driver.findElement(locator).isEnabled() == false) {
				TestInfo.getTest().log(LogStatus.PASS, "Element: " + locator + " is disabled...", ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));
			} else {
				TestInfo.getTest().log(LogStatus.FAIL, "Element: " + locator + " is enabled...", ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
				Assert.fail("Element is enabled...");
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void actionClickElement(final By locator) {
		try {

			Actions action = new Actions(driver);
			WebElement path = driver.findElement(locator);
			action.click(path).perform();

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void clickElement(final By locator) {
		try {

			((JavascriptExecutor) driver).executeScript(
					"var elem=arguments[0]; setTimeout(function() {elem.click();}, 1000)",
					driver.findElement(locator));
			TestInfo.getTest().log(LogStatus.INFO, "Successfully clicked element...", ExtentManager.statusLableStyle(ReportStatusLabel.INFO));

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void selectTile(String tile) {
		try {
			
			((JavascriptExecutor) driver).executeScript(
					"var elem=arguments[0]; setTimeout(function() {elem.click();}, 1000)",
					driver.findElement(By.xpath("*//div[contains(@class, 'pending_mobile')][contains(@class, '"+ tile +"')]")));
			TestInfo.getTest().log(LogStatus.INFO, "Successfully select element...", ExtentManager.statusLableStyle(ReportStatusLabel.INFO));

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void selectDropdownOptionByText(final By locator, String value) {
		try {
			
			Select drpCountry = new Select(driver.findElement(locator));
			drpCountry.selectByVisibleText(value);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void selectDropdownOptionByValue(final By locator, String value) {
		try {
			
			Select drpCountry = new Select(driver.findElement(locator));
			drpCountry.selectByValue(value);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void selectDropdownOptionByIndex(final By locator, int value) {
		try {
			
			Select drpCountry = new Select(driver.findElement(locator));
			drpCountry.selectByIndex(value);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void multipleClickElement(final By locator, int times) {

		try {

			for (int i = 0; i < times; i++) {
				clickElement(locator);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void verifyDisplayedElement(final By locator) {
		boolean isElementDisplayed;

		try {

			isElementDisplayed = driver.findElement(locator).isDisplayed();

			if (isElementDisplayed) {
				TestInfo.getTest().log(LogStatus.PASS, "Element: " + locator + " is displayed...", ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));
				LOGGER.info("Element is displayed...");

			} else {
				TestInfo.getTest().log(LogStatus.FAIL, "Element: " + locator + " is not displayed...", ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
				Assert.fail("Element is not displayed...");
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void verifyNotDisplayedElement(final By locator) {
		boolean isElementDisplayed;

		try {

			isElementDisplayed = driver.findElement(locator).isDisplayed();

			if (isElementDisplayed == false) {
				TestInfo.getTest().log(LogStatus.PASS, "Element: " + locator + " is not displayed...", ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));

			} else {
				TestInfo.getTest().log(LogStatus.FAIL, "Element: " + locator + " is displayed...", ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
				Assert.fail("Element is displayed...");
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void insertText(final By locator, String text) {
		try {

			driver.findElement(locator).sendKeys(text);
			TestInfo.getTest().log(LogStatus.INFO, StringQaHelper.append("Successfully insert text: ", text), ExtentManager.statusLableStyle(ReportStatusLabel.INFO));

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void clearText(final By locator) {
		try {

			driver.findElement(locator).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			driver.findElement(locator).sendKeys(Keys.BACK_SPACE);
			TestInfo.getTest().log(LogStatus.INFO, "Text successfully cleared...", ExtentManager.statusLableStyle(ReportStatusLabel.INFO));

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}

	public void containsText(final By locator, String text) {

		String sGetText;

		try {

			sGetText = driver.findElement(locator).getText();
			if (sGetText.trim().contains(text.trim())) {
				TestInfo.getTest().log(LogStatus.PASS, StringQaHelper.append("Expected: ", sGetText, " contains: ", "Actual: ", text), ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));
			} else {
				TestInfo.getTest().log(LogStatus.FAIL, StringQaHelper.append("Expected: ", sGetText, " does not contains: ", "Actual: ", text), ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
				Assert.fail(StringQaHelper.append("Expected: ", sGetText, " does not contains: ", "Actual: ", text));
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void verifyText(final By locator, String text) {

		String sGetText;

		try {

			sGetText = driver.findElement(locator).getText();
			if (sGetText.trim().equals(text.trim())) {
				TestInfo.getTest().log(LogStatus.PASS, "Expected: " + sGetText + " is equal to: " + "Actual: " + text, 
						ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));

			} else {
				TestInfo.getTest().log(LogStatus.FAIL, "Expected: " + sGetText + " is not equal to: " + "Actual: " + text, 
						ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
				Assert.fail("Expected: " + sGetText + " is not equal to: " + "Actual: " + text);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
	
	public void verifyCountTableList(final By locator, int count) {

		int iGetTotal;

		try {

			iGetTotal = driver.findElements(locator).size();
			
			if(count == iGetTotal) {
				TestInfo.getTest().log(LogStatus.PASS, "Expected: " + iGetTotal + " is equal to: " + "Actual: " + count, 
						ExtentManager.statusLableStyle(ReportStatusLabel.PASSED));
			} else {
				TestInfo.getTest().log(LogStatus.FAIL, "Expected: " + iGetTotal + " is not equal to: " + "Actual: " + count, 
						ExtentManager.statusLableStyle(ReportStatusLabel.FAILED));
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			TestInfo.getTest().log(LogStatus.FAIL, e.getMessage());
			Assert.fail();
		}
	}
}
