package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface TextCommand {
	
	void VERIFY_LABEL(String text);
	
}
