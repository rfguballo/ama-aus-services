package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface EnabledCommand {

	 void VERIFY_IF_ENABLED();
	
	 void VERIFY_IF_DISABLED();

	 void CLICK();
	
}
