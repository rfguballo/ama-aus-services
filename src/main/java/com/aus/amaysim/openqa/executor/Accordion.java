package com.aus.amaysim.openqa.executor;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.command.ClickCommand;
import com.aus.amaysim.openqa.command.DisplayCommand;
import com.aus.amaysim.openqa.handler.NavigateElement;

/**
 * @author rguballo
 *
 */
public class Accordion implements ClickCommand, DisplayCommand {

    private final By locator;

	public Accordion(final By locator) {
		this.locator = locator;
	}
	
	@Override
	public void CLICK() {
		NavigateElement.executeInstance().clickElement(locator);
	}

	@Override
	public void ACTION_CLICK() {
		NavigateElement.executeInstance().actionClickElement(locator);
		
	}

	@Override
	public void MULTIPLE_CLICK(int times) {
		NavigateElement.executeInstance().multipleClickElement(locator, times);
	}

	@Override
	public void VERIFY_IF_DISPLAYED() {
		NavigateElement.executeInstance().verifyDisplayedElement(locator);
	}

	@Override
	public void VERIFY_IF_NOT_DISPLAYED() {
		NavigateElement.executeInstance().verifyNotDisplayedElement(locator);
	}
}
