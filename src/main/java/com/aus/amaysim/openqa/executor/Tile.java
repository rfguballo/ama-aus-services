package com.aus.amaysim.openqa.executor;

import com.aus.amaysim.openqa.command.SelectCommand;
import com.aus.amaysim.openqa.handler.NavigateElement;

/**
 * @author rguballo
 *
 */
public class Tile implements SelectCommand {
	

	@Override
	public void SELECT_TILE(String value) {

		NavigateElement.executeInstance().selectTile(value);
		
	}
}
