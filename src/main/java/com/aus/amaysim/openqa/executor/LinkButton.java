package com.aus.amaysim.openqa.executor;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.command.ClickCommand;
import com.aus.amaysim.openqa.command.DisplayCommand;
import com.aus.amaysim.openqa.command.EnabledCommand;
import com.aus.amaysim.openqa.command.TextCommand;
import com.aus.amaysim.openqa.handler.NavigateElement;

/**
 * 
 * @description: Commands for button actions
 * @author: rguballo
 * @date: Apr 25, 2018
 *
 */
public class LinkButton implements ClickCommand, EnabledCommand, DisplayCommand, TextCommand {

    private final By locator;

	public LinkButton(final By locator) {
		this.locator = locator;
	}

	@Override
	public void CLICK() {
		NavigateElement.executeInstance().clickElement(locator);
	}

	@Override
	public void ACTION_CLICK() {
		NavigateElement.executeInstance().actionClickElement(locator);
		
	}

	@Override
	public void MULTIPLE_CLICK(int times) {
		NavigateElement.executeInstance().multipleClickElement(locator, times);
	}

	@Override
	public void VERIFY_IF_ENABLED() {
		NavigateElement.executeInstance().verifyEnabledElement(locator);
	}

	@Override
	public void VERIFY_IF_DISABLED() {
		NavigateElement.executeInstance().verifyDisabledElement(locator);
	}

	@Override
	public void VERIFY_IF_DISPLAYED() {
		NavigateElement.executeInstance().verifyDisplayedElement(locator);
	}

	@Override
	public void VERIFY_IF_NOT_DISPLAYED() {
		NavigateElement.executeInstance().verifyNotDisplayedElement(locator);
	}

	@Override
	public void VERIFY_LABEL(String text) {
		NavigateElement.executeInstance().verifyText(locator, text);
	}
}
