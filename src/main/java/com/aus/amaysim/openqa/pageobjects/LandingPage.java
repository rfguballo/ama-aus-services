package com.aus.amaysim.openqa.pageobjects;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.executor.LinkButton;

/**
 * @author rguballo
 *
 */
public class LandingPage {

	public static class HeaderBar {
		public static final LinkButton login = new LinkButton(By.xpath("*//a[contains(@href, 'login')]/descendant::span[contains(text(), 'Login')]"));
	}
}
