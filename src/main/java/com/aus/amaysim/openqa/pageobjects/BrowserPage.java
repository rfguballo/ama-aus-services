package com.aus.amaysim.openqa.pageobjects;

import com.aus.amaysim.openqa.handler.NavigateElement;

/**
 * @author rguballo
 *
 */
public class BrowserPage {

	public static void QUIT_BROWSER() {
		NavigateElement.executeInstance().quitBrowser();
	}
	
	public static void WAIT_FOR_COMPLETE_LOAD() {
		NavigateElement.executeInstance().watiForAjaxToLoad();
	}
}
