package com.aus.amaysim.openqa.pageobjects;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.executor.Text;
import com.aus.amaysim.openqa.executor.TextBox;

/**
 * @author rguballo
 *
 */
public class MobileHelpPage {

	public static class Header {
		public static final TextBox findMobileHelp = new TextBox(By.xpath("*//div[contains(@id, 'block-searchblock-search')]/descendant::input[contains(@placeholder, 'Find mobile help...')]"));
		public static final Text autocomplete_SearchValue = new Text(By.xpath("*//a[contains(@class, 'inb-tt-suggestion')][1]"));
	}
	
	public static class BlogHighlight {
		
		public static final Text blogTitle = new Text(By.xpath("*//h1[contains(@class, 'page-title')]/span"));
	}
}
