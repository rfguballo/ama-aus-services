package com.aus.amaysim.openqa.pageobjects;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.executor.Text;
import com.aus.amaysim.openqa.executor.Tile;

/**
 * @author rguballo
 *
 */
public class ServicePage {

	public static class MobilePlan {
		
		public static final Text mobilePlan_Header = new Text(By.xpath("*//h2[contains(@data-automation-id, 'mobile-heading')]"));
		public static final Tile serviceTileMobile = new Tile();
		public static final Text newMobilePlan = new Text(By.xpath("*//h3[contains(@id, 'add_service_heading_container_mobile')]"));
	}
	
	public static class EnergyPlan {
		
		public static final Text switchToday = new Text(By.xpath("*//h3[contains(@id, 'add_service_heading_container_energy')]"));
	}
}
